const express = require('express')
const router = express.Router()
const jwt = require('jsonwebtoken');

const {getUser} = require('../services/userService')
const {patchUser} = require('../services/userService')

router.get('/me', async (req, res) => {
	try {
		const {
			authorization
		} = req.headers;

		if (!authorization) throw 400

		const [, token] = authorization.split(' ');
		const tokenPayload = jwt.verify(token, 'secret');
		const email = tokenPayload.email
		const user = await getUser(email)

		res.json({
			user: {
				_id: user._id,
				role: user.role,
				email: user.email,
				created_date: user.created_date
			}
		})
	} catch(err) {
		if (err == 400) res.status(400).json('bad request')
		else res.status(500).json(err.message)
	}
})

router.patch('/me', async (req, res) => {
	try {
		const {
			authorization
		} = req.headers;

		if (!authorization) throw '!authorization'

		const [, token] = authorization.split(' ');
		const tokenPayload = jwt.verify(token, 'secret');
		const email = tokenPayload.email
		const password = req.body.newPassword

		await patchUser(email, password)
		res.json('Password changed successfully')
	} catch(err) {
		res.status(400).json(err)
	}
})

module.exports = {
	userRouter: router
}