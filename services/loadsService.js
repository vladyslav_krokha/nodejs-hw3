const {Load} = require('../models/loadModel');

const getLoads = async (userId) => {
    const loads = await Load.find({created_by: userId});
    return loads;
}

const addLoad = async (userId, loadPayload) => {
    const load = new Load({created_by: userId, ...loadPayload});
    await load.save();
}

const getLoadById = async (loadId) => {
    const load = await Load.findOne({_id: loadId})
    return load
}

const getActiveLoadByDriverId = async (driverId) => {
    const load = await Load.findOne({assigned_to: driverId})
    return load
}

const updateLoad = async (loadId, data) => {
    await Load.findOneAndUpdate({_id: loadId}, {$set: data})
}

const postLoad = async (loadId) => {
    await Load.findOneAndUpdate({_id: loadId}, {$set: {status: 'POSTED'}})
}

const deleteLoad = async (loadId) => {
    await Load.findByIdAndDelete({_id: loadId})
}

module.exports = {
    getLoads,
    addLoad,
    getLoadById,
    updateLoad,
    postLoad,
    getActiveLoadByDriverId,
    deleteLoad
};