const express = require('express');
const router = express.Router();

const {
    getLoads,
    addLoad,
    getLoadById,
    updateLoad,
    postLoad,
    getActiveLoadByDriverId,
    deleteLoad
} = require('../services/loadsService');

const {
    getTrucks,
    updateTruck
} = require('../services/trucksService')

router.get('/', async (req, res) => {
    try {
        const { userId, role } = req.user;
        
        if (role !== 'SHIPPER') throw 'user isn`t shipper'
        
        const loads = await getLoads(userId);

        if (!loads) throw 'loads not found'
    
        res.json({loads});    
    } catch(err) {
        res.status(400).json(err)
    }
});

router.post('/', async (req, res) => {
    try {
        const { userId, role } = req.user;
        
        if (role !== 'SHIPPER') throw 'user isn`t shipper'

        await addLoad(userId, req.body);
    
        res.json({message: 'Success'});    
    } catch(err) {
        res.status(400).json(err)
    }
});

router.get('/active', async (req, res) => {
    try {
        const { userId, role } = req.user

        if (role !== 'DRIVER') throw 'user isn`t driver'

        const activeLoad = await getActiveLoadByDriverId(userId)

        if (!activeLoad) throw 'no active load'

        res.json({
            load: activeLoad
        })
    } catch (err) {
        res.status(400).json(err)
    }
})

router.get('/:id', async (req, res) => {
    try {
        const { id } = req.params
        
        if (req.user.role !== 'SHIPPER') throw 'user isn`t shipper'
        
        const load = await getLoadById(id);
    
        res.json(load);    
    } catch(err) {
        res.status(400).json(err)
    }
});

router.put('/:id', async (req, res) => {
    try {
        const { role } = req.user
        const { id } = req.params
        const data = req.body

        if (role !== 'SHIPPER') throw 'user isn`t shipper'

        const load = await getLoadById(id)

        if (load.status !== 'NEW') throw 'load was posted'

        await updateLoad(id, data)
        res.json('load updated successfully')
    } catch (err) {
        res.status(400).json(err)
    }
})

router.post('/:id/post', async (req, res) => {
    try {
        const { role } = req.user
        const { id } = req.params

        if (role !== 'SHIPPER') throw 'user isn`t shipper'

        const load = await getLoadById(id)

        if (load.status !== 'NEW') throw 'load was posted'

        await postLoad(id)
        const trucks = await getTrucks()
        const truck = trucks.find(truck => truck.status === 'IS')

        if (!truck) updateLoad(id, {type: 'NEW'})

        await updateTruck(truck._id, {status: 'OL'})
        await updateLoad(id, {status: 'ASSIGNED', assigned_to: truck.assigned_to, state: 'En route to Pick Up'})

        res.json({
            "message": "Load posted successfully",
            "driver_found": true
        })
    } catch (err) {
        res.status(400).json(err)
    }
})

router.patch('/active/state', async (req, res) => {
    try {
        const { userId, role} = req.user
        const loadStates = [null, 'En route to Pick Up', 'Arrived to Pick Up', 'En route to delivery', 'Arrived to delivery']

        if (role !== 'DRIVER') throw 'user isn`t driver'

        const activeLoad = await getActiveLoadByDriverId(userId)

        if (!activeLoad) throw 'no active load'

        currentStateIndex = loadStates.indexOf(activeLoad.state)

        if (currentStateIndex == loadStates.length - 1) throw 'load arrived to delivery'

        if (activeLoad.state === 'En route to delivery') {
            updateLoad(activeLoad._id, {status: 'SHIPPED', state: loadStates[++currentStateIndex]})
        } else {
            updateLoad(activeLoad._id, {state: loadStates[++currentStateIndex]})
        }

        res.json({"message": `Load state changed to ${loadStates[++currentStateIndex]}`})
    } catch (err) {
        res.status(400).json(err)
    }
})

router.delete('/:id', async (req, res) => {
    try {
        const { id } = req.params

        if (req.user.role !== 'SHIPPER') throw 'user isn`t shipper'
        
        const load = await getLoadById(id);

        if (load.status !== 'NEW') throw 'load was posted'

        await deleteLoad(id)
        res.json('Load deleted successfully');    
    } catch(err) {
        res.status(400).json(err)
    }
});

module.exports = {
    loadsRouter: router
}