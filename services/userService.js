const bcrypt = require('bcrypt')
const {User} = require('../models/userModel');

const getUser = async (email) => {
	const user = await User.findOne({email})

	if (!user) {
			throw new Error('No users found');
	}

	return user
}

const patchUser = async (email, password) => {
	const user = await User.findOne({email})

	if (!user) throw new Error('No users found');

	user.password = await bcrypt.hash(password, 10)
	await user.save()
}

module.exports = {
	getUser,
	patchUser
}